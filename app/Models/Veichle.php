<?php

namespace App\Models;

use App\Models\Event;
use App\Models\Report;
use App\Models\Revision;
use App\Models\Insurance;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Veichle extends Model
{
    use HasFactory;

    protected $fillable= [
        'nome',
        'intestazione',
        'sospensione',
        'gomme_neve',
        'km',
        'targa',
    ];
    public function insurance(){

        return $this->belongsTo(Insurance::class);
    }

    public function event(){

        return $this->hasMany(Event::class);
    }

    public function report(){

        return $this->hasMany(Report::class);
    }

    public function revision(){

        return $this->hasMany(Revision::class);
    }
}
