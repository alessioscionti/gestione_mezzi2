<?php

namespace App\Models;

use App\Models\Veichle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Revision extends Model
{
    use HasFactory;

    protected $fillable=[
        'data_scadenza',
        'costo',
    ];

    public function veichle(){

        return $this->belongsTo(Veichle::class);
    }
}
