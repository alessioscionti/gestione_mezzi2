<?php

namespace App\Models;

use App\Models\Veichle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Report extends Model
{
    use HasFactory;
    protected $fillable=[
        'user_id',
        'veichle_id',
        'motivo_segnalazione',
        'motivo_segnalazione',
        'data_segnalazione',
        'dettaglio_segnalazione',
        'immagine',
        'risolto',
        'km_attuali',
    ];

    public function veichle(){

        return $this->belongsTo(Veichle::class);
    }

    public function user(){

        return $this->belongsTo(User::class);
    }
}
