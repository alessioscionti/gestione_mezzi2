<?php

namespace App\Models;

use App\Models\Veichle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Insurance extends Model
{
    use HasFactory;
    protected $fillable=[
        'veichle_id',
        'compagnia',
        'assistenza',
        'data_scadenza',
        'data_attivazione',
        'costo',
        'sospensione',
    ];

    public function veichle(){

        return $this->belongsTo(Veichle::class);
    }
}
