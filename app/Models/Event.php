<?php

namespace App\Models;

use App\Models\Veichle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Event extends Model
{
    use HasFactory;
    protected $fillable=[
        'veichle_id',
        'data_evento',
        'descrizione',
        'costo',
        'km_percorsi', 
    ];

    public function veichle(){

        return $this->belongsTo(Veichle::class);
    }
}
