<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Insurance;
use App\Models\Report;
use App\Models\Veichle;
use Illuminate\Http\Request;

class VeichleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $veichles=Veichle::all();
        
        return view('veichles.index', compact('veichles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('veichles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $veichle=Veichle::create([
            'nome'=>$request->nome,
            'intestazione'=>$request->intestazione,
            'sospensione'=>$request->sospensione,
            'gomme_neve'=>$request->gomme_neve,
            'km'=>$request->km,
            'targa'=>$request->targa,
        ]);
        return redirect()->back()->with('message', 'Veicolo inserito correttamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Veichle  $veichle
     * @return \Illuminate\Http\Response
     */
    public function show(Veichle $veichle)
    {
        
        $insurance=Insurance::where('veichle_id','=',$veichle->id)->get();
        
        
        $events=Event::where('veichle_id','=',$veichle->id)->get();
        $reports=Report::where('veichle_id','=',$veichle->id)->get();
        return view('veichles.details',compact('veichle','insurance','events','reports'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Veichle  $veichle
     * @return \Illuminate\Http\Response
     */
    public function edit(Veichle $veichle)
    {
        /* $veichle=Veichle::where('id','=',$veichle->id)->get();
        dd($veichle); */
        
        
        return view('veichles.edit',compact('veichle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Veichle  $veichle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Veichle $veichle)
    {
        $veichle->nome=$request->nome;
        $veichle->intestazione=$request->intestazione;
        $veichle->sospensione=$request->sospensione;
        $veichle->gomme_neve=$request->gomme_neve;
        $veichle->km=$request->km;
        $veichle->targa=$request->targa;
        
        $veichle->save();
        

        $insurance=Insurance::where('veichle_id','=',$veichle->id)->get();
        $events=Event::where('veichle_id','=',$veichle->id)->get();
        $reports=Report::where('veichle_id','=',$veichle->id)->get();
        return view('veichles.details',compact('veichle','insurance','events','reports'))->with('message', 'veicolo modificato correttamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Veichle  $veichle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Veichle $veichle)
    {
        //
    }
}
