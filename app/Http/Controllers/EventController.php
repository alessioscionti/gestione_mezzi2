<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Veichle;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $veichles=Veichle::all();
        $events=Event::all();
        return view('event.index',compact('veichles','events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $veichles=Veichle::all();
        
        return view('event.create',compact('veichles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $events=Event::create([
            'veichle_id'=>$request->veichle_id,
            'data_evento'=>$request->data_evento,
            'descrizione'=>$request->descrizione,
            'costo'=>$request->costo,
            'km_percorsi'=>$request->km_percorsi,
        ]);
        $veichle=Veichle::find($request->veichle_id)->get();
        foreach($veichle as $veic){
            $km=$veic->km;
        }
        if ($km < $request->km_percorsi) {
            $updatekm=Veichle::find($request->veichle_id)->update(['km'=> $request->km_percorsi]);
        }
        return redirect()->back()->with('message', 'Intervento registrato correttamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $veichles=Veichle::where('id','=',$event->veichle_id)->get();
        
        return view('event.edit',compact('event','veichles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        /* dd($request,$event); */
        $event->veichle_id=$request->veichle_id;
        $event->data_evento=$request->data_evento;
        $event->descrizione=$request->descrizione;
        $event->costo=$request->costo;
        $event->km_percorsi=$request->km_percorsi;
        $event->save();

        $veichle=Veichle::find($request->veichle_id)->get();
        foreach($veichle as $veic){
            $km=$veic->km;
        }
        if ($km < $request->km_percorsi) {
            $updatekm=Veichle::find($request->veichle_id)->update(['km'=> $request->km_percorsi]);
        }
        $events=Event::all();
        $veichles=Veichle::all();
        return view('event.index', compact('events','veichles'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
