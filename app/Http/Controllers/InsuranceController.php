<?php

namespace App\Http\Controllers;

use App\Models\Insurance;
use App\Models\Veichle;
use Illuminate\Http\Request;

class InsuranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insurance=Insurance::orderBy('veichle_id', 'ASC')->paginate(10);
        $veichles=Veichle::all();
        return view('assicurazioni.index',compact('insurance','veichles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $veichles=Veichle::all();
        return view('assicurazioni.create',compact('veichles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* dd($request); */
        $insurance=Insurance::create([
            'veichle_id'=>$request->veichle_id,
            'compagnia'=>$request->compagnia,
            'assistenza'=>$request->assistenza,
            'data_attivazione'=>$request->data_attivazione,
            'data_scadenza'=>$request->data_scadenza,
            'costo'=>$request->costo,
            'sospensione'=>$request->sospensione,
        ]);
        return redirect()->back()->with('message', 'Assicurazione registrata correttamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Insurance  $insurance
     * @return \Illuminate\Http\Response
     */
    public function show(Insurance $insurance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Insurance  $insurance
     * @return \Illuminate\Http\Response
     */
    public function edit(Insurance $insurance)
    {
        $veichles=Veichle::where('id','=',$insurance->veichle_id)->get();
        
        return view('assicurazioni.edit',compact('insurance','veichles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Insurance  $insurance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Insurance $insurance)
    {
        
        $insurance->veichle_id=$request->veichle_id;
        $insurance->compagnia=$request->compagnia;
        $insurance->assistenza=$request->assistenza;
        $insurance->data_attivazione=$request->data_attivazione;
        $insurance->data_scadenza=$request->data_scadenza;
        $insurance->costo=$request->costo;
        $insurance->sospensione=$request->sospensione;

        $insurance->save();
        $veichles=Veichle::all();
        $insurance=Insurance::all();
        return view('assicurazioni.index', compact('insurance','veichles'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Insurance  $insurance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Insurance $insurance)
    {
        //
    }
}
