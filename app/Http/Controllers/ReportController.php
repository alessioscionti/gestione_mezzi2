<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\User;
use App\Models\Veichle;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports=Report::all();
        return view('report.index',compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $veichles=Veichle::all();
        $users=User::all();
        
        return view('report.create',compact('veichles','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* dd($request); */
        
        if ($request->has('immagine')) {
            
            $report=Report::create([
                'veichle_id'=>$request->veichle_id,
                'user_id'=>$request->user_id,
                'motivo_segnalazione'=>$request->motivo_segnalazione,
                'data_segnalazione'=>$request->data_segnalazione,
                'dettaglio_segnalazione'=>$request->dettaglio_segnalazione,
                'km_attuali'=>$request->km_attuali,
                /* 'immagine'=>$request->file('immagine')->store('public/'), */
            ]);
            $veichle=Veichle::find($request->veichle_id)->get();
            foreach($veichle as $veic){
                $km=$veic->km;
            }
            if ($km < $request->km_attuali) {
                $updatekm=Veichle::find($request->veichle_id)->update(['km'=> $request->km_attuali]);
            }
            $idreport=$report->id;
            $idveichle=$report->veichle_id;
            $immagine=Report::where('id','=',$idreport)->update([
                'immagine'=>$request->file('immagine')->storeAs('/public/storage/'.$idreport.'', $idreport.'-'.$idveichle.'.jpg'),
            ]);
            /* dd($immagine); */
        }else{
            $report=Report::create([
                'veichle_id'=>$request->veichle_id,
                'user_id'=>$request->user_id,
                'motivo_segnalazione'=>$request->motivo_segnalazione,
                'data_segnalazione'=>$request->data_segnalazione,
                'dettaglio_segnalazione'=>$request->dettaglio_segnalazione,
                'km_attuali'=>$request->km_attuali,
                
            ]);
            $veichle=Veichle::where('id','=',$request->veichle_id)->get();
            foreach($veichle as $veic){
                $km=$veic->km;
            }
            if ($km < $request->km_attuali) {
                $updatekm=Veichle::find($request->veichle_id)->update(['km'=> $request->km_attuali]);
            }
        }
        return redirect()->back()->with('message', 'Report inserito correttamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        $veichles=Veichle::where('id','=',$report->veichle_id)->get();
        $users=User::where('id','=',$report->user_id)->get();
        
        return view('report.edit',compact('report','veichles', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        $report->user_id=$request->user_id;
        $report->veichle_id=$request->veichle_id;
        $report->motivo_segnalazione=$request->motivo_segnalazione;
        $report->data_Segnalazione=$request->data_segnalazione;
        $report->dettaglio_segnalazione=$request->dettaglio_segnalazione;
        $report->risolto=$request->risolto;
        $report->km_attuali=$request->km_attuali;
        
        $report->save();
        $veichle=Veichle::where('id','=',$request->veichle_id)->get();
        
        foreach($veichle as $veic){
            $km=$veic->km;
            
        }
        
        if ($km < $request->km_attuali) {
            $updatekm=Veichle::find($request->veichle_id)->update(['km'=> $request->km_attuali]);
        }

        $reports=Report::all();
        $users=User::all();
        $veichles=Veichle::all();
        return view('report.index', compact('reports','report','veichles','users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        //
    }
}
