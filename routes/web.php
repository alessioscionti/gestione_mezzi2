<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\VeichleController;
use App\Http\Controllers\InsuranceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home')->middleware('auth');

//gestione mezzi
Route::get('/mezzi/all',[VeichleController::class,'index'])->name('veichles.index');
Route::get('/mezzo/dettagli/{veichle}',[VeichleController::class,'show'])->name('veichles.details');
Route::get('/mezzo/edit/{veichle}',[VeichleController::class,'edit'])->name('veichles.edit');
Route::post('/mezzo/update/{veichle}',[VeichleController::class,'update'])->name('veichles.update');
Route::get('/mezzo/crea',[VeichleController::class,'create'])->name('veichles.create');
Route::Post('/mezzo/store',[VeichleController::class,'store'])->name('veichles.store');

//gestione utenze
Route::get('/users/all',[UsersController::class,'index'])->name('user.show');
Route::get('/user/edit/{user}',[UsersController::class,'edit'])->name('user.edit');
Route::post('/user/update/{user}',[UsersController::class,'update'])->name('user.update');

//gestione assicurazioni
Route::get('/assicurazioni/all',[InsuranceController::class,'index'])->name('assicurazioni.index');
Route::get('/assicurazione/crea',[InsuranceController::class,'create'])->name('assicurazioni.create');
Route::post('/assicurazione/store',[InsuranceController::class,'store'])->name('assicurazioni.store');
Route::get('/assicurazione/edit/{insurance}',[InsuranceController::class,'edit'])->name('assicurazioni.edit');
Route::post('/assicurazione/update/{insurance}',[InsuranceController::class,'update'])->name('assicurazioni.update');



//gestione eventi/interventi mezzo
Route::get('/interventi/all',[EventController::class,'index'])->name('event.index');
Route::get('/intervento/crea',[EventController::class,'create'])->name('event.create');
Route::post('/intervento/store',[EventController::class,'store'])->name('event.store');
Route::get('/intervento/edit/{event}',[EventController::class,'edit'])->name('event.edit');
Route::post('/intervento/update/{event}',[EventController::class,'update'])->name('event.update');

//gestione report

Route::get('/reports/all',[ReportController::class,'index'])->name('report.index');
Route::get('/report/crea',[ReportController::class,'create'])->name('report.crea');
Route::post('/report/store',[ReportController::class,'store'])->name('report.store');
Route::get('/report/edit/{report}',[ReportController::class,'edit'])->name('report.edit');
Route::post('/report/update/{report}',[ReportController::class,'update'])->name('report.update');





