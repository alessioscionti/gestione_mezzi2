<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('veichle_id');
            $table->foreign('veichle_id')->references('id')->on('veichles');
            $table->text('compagnia');
            $table->string('assistenza',50)->nullable();
            $table->date('data_attivazione');
            $table->date('data_scadenza');
            $table->decimal('costo',10,2);
            $table->boolean('sospensione')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurances');
    }
}
