<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('veichle_id');
            $table->foreign('veichle_id')->references('id')->on('veichles');
            $table->text('motivo_segnalazione')->nullable();
            $table->date('data_segnalazione');
            $table->text('dettaglio_segnalazione')->nullable();
            $table->string('immagine')->nullable();
            $table->boolean('risolto')->nullable();
            $table->bigInteger('km_attuali');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
