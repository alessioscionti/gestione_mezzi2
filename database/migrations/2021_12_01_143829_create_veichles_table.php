<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVeichlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veichles', function (Blueprint $table) {
            $table->id();
            $table->text('nome');
            $table->text('intestazione');
            $table->boolean('sospensione')->nullable();
            $table->boolean('gomme_neve')->nullable();
            $table->bigInteger('km');
            $table->text('targa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veichles');
    }
}
