<x-layout>
  <div style="height: 200px"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <main id='main' class="mt-5">
        <h1 id='title'>Registrazione Utente</h1>
        <p id='description'></p>
        <figure>
          <form id='survey-form' method="POST" action="{{route('register')}}">
            @csrf
            <label id='name-label'>Nome</label>
            <input type='text' id='name' name="name" placeholder='Username' class='control' required>
            <hr>
            <label id='email-label'>Email</label>
            <input type='email' id='email' placeholder='Email' class='control' name="email" required>
            <hr>
            <label id='number-label'>Password</label>
            <input type='password' id='text' min='5' max='110' class='control' placeholder='password..' name="password" required>
            <hr>
            <label id='number-label'>Conferma Password</label>
            <input type='password' id='text' min='5' max='110' class='control' placeholder='password..' name="password_confirmation" required>
            <hr>
            <button id="submit">Salva</button>
      
          </form>
        </figure>
      </main>


</x-layout>