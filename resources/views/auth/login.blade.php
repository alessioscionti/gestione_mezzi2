<x-layout>
    <div style="height: 200px"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            </div>
        </div>
    </div>
    <main id='main' class="mt-5">
        <h1 id='title'>Login</h1>
        <p id='description'></p>
        <figure>
          <form id='survey-form' method="POST" action="{{route('login')}}">
            @csrf
            <label id='name-label'>Nome</label>
            <input type='email' id='name' name="email" placeholder='Nome Mezzo' class='control' required>
            <hr>
            <label id='number-label'>Password</label>
            <input type='password' id='text' min='5' max='110' class='control' placeholder='password..' name="password" required>
            <hr>
            <button id="submit">Salva</button>
      
          </form>
        </figure>
      </main>


</x-layout>