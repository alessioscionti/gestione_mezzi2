<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <title>Gestione Mezzi</title>
</head>
<style>
    html,
    body {
    background: rgb(116,115,121);
    /* background: linear-gradient(180deg, rgba(116,115,121,1) 0%, rgba(15,15,15,1) 100%); */
    font-family: 'Poppins';
    /* text-align: center; */
    color: white;
    /* margin: 0px; */
}
</style>

<body>

    <x-navbar/>
    {{$slot}}


    <script src="{{asset('js/app.js')}}"></script>

</body>

</html>