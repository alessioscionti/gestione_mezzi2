<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <main id='main' class="mt-5">
        <h1 id='title'>Registra Nuova Assicurazione</h1>
        <p id='description'></p>
        <figure>
          <form id='survey-form' method="POST" action="{{route('assicurazioni.update',$insurance)}}">
            @csrf
            <label id='name-label'>Modifica Assicurazione:</label>
            <select name="veichle_id" id="">
                @foreach ($veichles as $veichle)
                
                <option value="{{$veichle->id}}">{{$veichle->nome}} - {{$veichle->targa}}</option>
                @endforeach
            </select>
            <hr>
            <label id='email-label'>Compagnia Assicurativa</label>
            <input type='text' id='email' class='control' name="compagnia" value="{{$insurance->compagnia}}" required>
            <hr>
            <label id='email-label'>Numero Assistenza Stradale</label>
            <input type='text' id='email' class='control' name="assistenza" value="{{$insurance->assistenza}}" required>
            <hr>
            <label id='email-label'>Data attivazione</label>
            <input type='date' id='email' class='control' name="data_attivazione" value="{{$insurance->data_attivazione}}" required>
            <hr>
            <label id='number-label'>Data Scadenza</label>
            <input type='date' id='number' class='control' name="data_scadenza" value="{{$insurance->data_scadenza}}" required>
            <hr>
            <label id='number-label'>Costo</label>
            <input type='number' id='number' class='control' name="costo" placeholder="€" value="{{$insurance->costo}}" required>
            <hr>
            @if ($insurance->sospensione==null||0)
            <label id='number-label'>Sospensione Veicolo</label>
            <input type='checkbox' id='number' class='control' name="sospensione" value="1"  style="width:20px;height:20px;">
            <hr>
            @else
            <label id='number-label'>Sospensione Veicolo</label>
            <input type='checkbox' id='number' class='control' name="sospensione" value="1"  style="width:20px;height:20px;" checked>
            <hr>
            @endif
            <button id="submit">Salva</button>
      
          </form>
        </figure>
      </main>


</x-layout>