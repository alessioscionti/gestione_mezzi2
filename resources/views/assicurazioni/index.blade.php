<style>
    td{
        color: white;
    }
</style>
<x-layout>
    <div style="height: 200px"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 mt-5">
                <button class="btn btn-success"><a href="{{route('assicurazioni.create')}}" style="text-decoration: none;color:white">Registra Nuova Assicurazione</a></button>
            </div>
        </div>
    </div>
        <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                @if (Session::has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
                </div>
            </div>
            <div class="row">
                  <div class="col-12">
                      <table class="table">
                          <thead>
                            <tr>
                                <th>Compagnia assicurativa</th>
                                <th>Mezzo</th>
                                <th>Numero assistenza Stradale</th>
                                <th>Data Attivazione</th>
                                <th>Data Scadenza</th>
                                <th>Costo</th>
                                <th>Sospesa</th>
                                <th>Azioni</th>
                            </tr>
                        </thead>
                      <tbody>
                        @foreach ($insurance as $ins)
                        <tr>
                          <td>{{$ins->compagnia}}</td>
                          <td>{{$ins->veichle->nome}}</td>
                          <td>{{$ins->assistenza}}</td>
                          <td>{{$ins->data_attivazione}}</td>
                          <td>{{$ins->data_attivazione}}</td>
                          <td>€ {{$ins->costo}}</td>
                          @if ($ins->sospensione==null||0)
                            <td>No</td>
                          @else
                          <td>Si</td>
                          @endif
                          <td><a href="{{route('assicurazioni.edit',$ins->id)}}"><i class="fas fa-edit" style="color:red"></i></a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
{{--                     <div class="container">
                        <div class="row">
                            <div class="col">
                                {{ $insurance->links() }}
                                <p>Assicurazioni {{$insurance->count()}} di {{ $insurance->total() }}.</p>
        
                            </div>
                        </div>
                    </div> --}}
                  </div>
              </div>
          </div>
</x-layout>