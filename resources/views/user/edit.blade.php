<x-layout>
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <main id='main' class="mt-5">
        <h1 id='title'>Registrazione Utente</h1>
        <p id='description'></p>
        <figure>
          <form id='survey-form' method="POST" action="{{route('user.update',$user)}}">
            @csrf
            <label id='name-label'>Nome</label>
            <input type='text' id='name' name="name" placeholder='Username' class='control' value="{{$user->name}}">
            <hr>
            @if ($user->admin==1)
            <label id='name-label'>Admin</label>
            <input type='checkbox' id='name' name="admin" class='control' checked>
            <hr>
            @else
            <label id='name-label'>Admin</label>
            <input type='checkbox' id='name' name="admin" class='control'>
            <hr>
            @endif
            @if ($user->worker==1)
            <label id='name-label'>Guida</label>
            <input type='checkbox' id='name' name="worker" class='control' checked>
            <hr>
            @else
            <label id='name-label'>Guida</label>
            <input type='checkbox' id='name' name="worker" class='control'>
            <hr>
            @endif
            <button id="submit">Salva</button>
      
          </form>
        </figure>
      </main>


</x-layout>