<x-layout>
  <div style="height: 300px"></div>
    @guest
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <main id='main' class="mt-5">
        <h1 id='title'>Login</h1>
        <p id='description'></p>
        <figure>
          <form id='survey-form' method="POST" action="{{route('login')}}">
            @csrf
            <label id='name-label'>Nome</label>
            <input type='email' id='name' name="email" placeholder='Nome Mezzo' class='control' required>
            <hr>
            <label id='number-label'>Password</label>
            <input type='text' id='text' min='5' max='110' class='control' placeholder='password..' name="password" required>
            <hr>
            <button id="submit">Salva</button>
      
          </form>
        </figure>
      </main>
        
    @else
      <div class="container">
          <div class="row">
              <div class="col-12">
                  <table class="table">
                      <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Ruolo</th>
                            <th>Azioni</th>
                        </tr>
                    </thead>
                  <tbody>
                    @foreach ($users as $user)
                    <tr>
                      <td>{{$user->name}}</td>
                      <td>{{$user->email}}</td>
                      @if ($user->admin)
                      <td>Admin</td>
                      @else
                      <td>User</td>
                      @endif
                      @if (Auth::user()->admin)
                      <td><a href="{{route('user.edit',$user->id)}}"><i class="fas fa-edit" style="color: white"></i></a></td>
                      @endif
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
          </div>
      </div>
    @endguest
    </x-layout>