<style>
    td{
        color: white;
    }
</style>
<x-layout>
    <div style="height: 200px"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 mt-5">
                <button class="btn btn-success"><a href="{{route('event.create')}}" style="text-decoration: none;color:white">Registra Nuovo Intervento</a></button>
            </div>
        </div>
    </div>
        <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                @if (Session::has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
                </div>
            </div>
            <div class="row">
                  <div class="col-12">
                      <table class="table">
                          <thead>
                            <tr>
                                <th>Mezzo</th>
                                <th>Data Intervento</th>
                                <th>Descrizione Intervento</th>
                                <th>Costo</th>
                                <th>Km Percorsi fino a intervento</th>
                                <th>Azioni</th>
                            </tr>
                        </thead>
                      <tbody>
                        @foreach ($events as $event)
                        <tr>
                          <td>{{$event->veichle->nome}}</td>
                          <td>{{$event->data_evento}}</td>
                          <td>{{$event->descrizione}}</td>
                          <td>€ {{$event->costo}}</td>
                          <td>{{$event->km_percorsi}}</td>
                          <td><a href="{{route('event.edit',$event->id)}}"><i class="fas fa-edit" style="color:red"></i></a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
          </div>
</x-layout>