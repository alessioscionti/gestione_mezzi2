<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <main id='main' class="mt-5">
        <h1 id='title'>Registra Nuovo Intervento</h1>
        <p id='description'></p>
        <figure>
          <form id='survey-form' method="POST" action="{{route('event.store')}}">
            @csrf
            <label id='name-label'>Intervento per il veicolo:</label>
            <select name="veichle_id" id="">
                @foreach ($veichles as $veichle)
                <option value="{{$veichle->id}}">{{$veichle->nome}} - {{$veichle->targa}}</option>
                @endforeach
            </select>
            <hr>
            <label id='email-label'>Data Operazione</label>
            <input type='date' id='email' class='control' name="data_evento" required>
            <hr>
            <label id='number-label'>Descrizione intervento</label>
            <input type='text' id='text' class='control' name="descrizione" required>
            <hr>
            <label id='number-label'>Costo</label>
            <input type='number' id='number' class='control' name="costo" placeholder="€" required>
            <hr>
            <label id='number-label'>Km al momento dell'intervento</label>
            <input type='number' id='number' class='control' name="km_percorsi" placeholder="km" required>
            <hr>
            <button id="submit">Salva</button>
      
          </form>
        </figure>
      </main>


</x-layout>