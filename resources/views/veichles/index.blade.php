<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    
    
    <div class="mybox-container justify-content-center">
      <a href="{{route('veichles.create')}}" style="text-decoration: none;color:white"><i class="fas fa-plus-circle" style="color:#198754;font-size:3rem"></i></a>
      <div class="row">
        @foreach ($veichles as $veichle)
        <div class="col-12 col-xs-12 col-md-12 col-lg-12 col-xl-3 mt-3">
            <div class="mybox mybox2">
              <div class="border"></div>
               <a href="{{route('veichles.details',compact('veichle'))}}"><img src="https://www.autotoday.it/wp-content/uploads/2021/06/Fiat-Ducato-elettrico.jpg" alt=""></a>
              <div class="caption bgopacity">
                
                <div class="content">
                  <span style="text-align: start;color:black">{{$veichle->nome}}</span>
                  <span style="text-align: start;color:black">Targa: {{$veichle->targa}}</span>
                </div>
              </div>
            </div>
        </div>
        @endforeach
      </div>
    </div>
</x-layout>