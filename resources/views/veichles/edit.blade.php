<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <main id='main' class="mt-5">
        <h1 id='title' class="d-flex justify-content-center">Modifica mezzo</h1>
        <p id='description'></p>
        <figure>
          <form id='survey-form' method="POST" action="{{route('veichles.update',$veichle)}}">
            @csrf
            <label id='name-label'>Nome</label>
            <input type='text' id='name' name="nome" placeholder='Nome Mezzo' class='control' value="{{$veichle->nome}}" required>
            <hr>
            <label id='name-label'>Intestazione azienda del mezzo</label>
            <input type='text' id='name' name="intestazione" placeholder='azienda intestazione mezzo' class='control' value="{{$veichle->intestazione}}" required>
            <hr>
            @if ($veichle->sospensione==1)
            <label id='number-label'>Sospensione Veicolo</label>
            <input type='checkbox' id='number' class='control' name="sospensione" value="1"  style="width:20px;height:20px;" checked>
            <hr>
            @else
            <label id='number-label'>Sospensione Veicolo</label>
            <input type='checkbox' id='number' class='control' name="sospensione" value="1"  style="width:20px;height:20px;">
            <hr>
            @endif
            @if ($veichle->gomme_neve==1)
            <label id='number-label'>Gomme neve</label>
            <input type='checkbox' id='number' class='control' name="gomme_neve" value="1" style="width:20px;height:20px;" checked>
            <hr>  
            @else
            <label id='number-label'>Gomme neve</label>
            <input type='checkbox' id='number' class='control' name="gomme_neve" value="1" style="width:20px;height:20px;">
            <hr>
            @endif
            <label id='email-label'>Km</label>
            <input type='number' id='email' placeholder='Km del mezzo' class='control' name="km" value="{{$veichle->km}}" required>
            <hr>
            <label id='number-label'>Targa</label>
            <input type='text' id='number' min='5' max='110' class='control' placeholder='ES0000ES' name="targa" value="{{$veichle->targa}}" required>
            <hr>
            <button id="submit">Salva</button>
      
          </form>
        </figure>
      </main>


</x-layout>