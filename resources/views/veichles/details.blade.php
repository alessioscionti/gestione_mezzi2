<style>
    h3, .h3 {
    font-size: calc(1.3rem + 0.6vw);
    margin-left: -6rem!important;
}

    ul, li {
    margin: 0;
    padding: 0;
    list-style: none;
    margin-left: -2.5rem!important;
}
.mybox {
    position: relative;
    width: 301px;
    height: 400px;
    background: #fff;
    border-radius: 1rem;
    box-shadow: 5px 2px 11px 2px rgb(0 0 0 / 30%);
    overflow: hidden;
    transition: 0.2s ease-in;
    margin-left: -6rem;
    
}
</style>
<x-layout>
    <div style="height: 100px"></div>
    <div class="container mt-5">
        <div class="row justify-content-center align-self-center" style="margin-left: 6rem">
            <div class="col" style="margin-right:6rem!important">
                <div class="mybox mybox2">
                    <div class="border">
                    </div>
                        <a href="{{route('veichles.index')}}"><img src="https://www.autotoday.it/wp-content/uploads/2021/06/Fiat-Ducato-elettrico.jpg" alt=""></a>
                    </div>
                    @if(Auth::user()->admin==1)
                    <a href="{{route('veichles.edit',$veichle->id)}}"><button class="btn btn-danger mt-2" style="margin-left: -1.3rem">Modifica Veicolo</button></a>
                    @endif
            </div>
            <div class="col">
                <h3 style="text-align: start">Nome: <p style="color: black">{{$veichle->nome}}</p></h3>
                <h3 style="text-align: start">Intestazione: <p style="color: black">{{$veichle->intestazione}}</p></h3>
                @if ($veichle->sospensione==null||0)
                <h3 style="text-align: start">Mezzo attivo: <p style="color: black">Attivo</p></h3>
                @else
                <h3 style="text-align: start">Mezzo attivo: <p style="color: black">Non attivo</p></h3>
                @endif
                @if ($veichle->gomme_neve==null||0)
                <h3 style="text-align: start">Gomme neve: <p style="color: black">Gomme neve non montate</p></h3>
                @else
                <h3 style="text-align: start">Gomme neve: <p style="color: black">Gomme neve Montate</p></h3>
                @endif
                <h3 style="text-align: start">Km: <p style="color: black">{{$veichle->km}}</p></h3>
                <h3 style="text-align: start">Targa: <p style="color: black">{{$veichle->targa}}</p></h3>
                <h3 style="text-align: start">Scadenza assicurazione: 
                @foreach ($insurance as $ins)
                @if ($ins->sospensione==1)
                <p style="color: black">Sospesa</p>
                @else
                @if($ins->data_scadenza<\Carbon\Carbon::now())
                <p style="color: red">In scadenza/scaduta</p>
                @endif
                    @if ($ins)
                    <p style="color: black">{{$ins->data_scadenza}}</p></h3>
                    @else
                    <p style="color: black">Nessuna Assicurazione</p></h3>
                    @endif
                @endif
                @endforeach  
                
                <h3 style="text-align: start">Interventi effettuati sul veicolo:</h3>
                <ul>
                    @foreach ($events as $event)
                    <li><p style="color: black">{{$event->descrizione}}<span style="color:white;"> In Data: </span><span style="color: black">{{$event->data_evento}}</span></li>
                    @endforeach
                </ul>
                <h3 style="text-align: start">Segnalazioni effettuate sul veicolo:</h3>
                <ul>
                    @foreach ($reports as $report)
                    <li>
                        <p>Utente: <span style="color: black">{{$report->user->name}}</span></p>
                        <p>Motivo segnalazione: <span style="color: black">{{$report->motivo_segnalazione}}</span></p>
                        <p>Dettaglio segnalazione: <span style="color: black">{{$report->dettaglio_segnalazione}}</span></p>
                        <p>Data segnalazione: <span style="color: black">{{$report->data_segnalazione}}</span></p>
                        <p>Foto segnalazione: <span style="color: black"><a href="{{asset('/storage/storage/'.$report->id.'/'.$report->id.'-'.$report->user->id.'.jpg')}}" data-lightbox="image-1" data-title="foto per: {{$report->motivo_segnalazione}} - {{$report->veichle->nome}} - segnalata da {{$report->user->name}}"> <img src="{{asset('/storage/storage/'.$report->id.'/'.$report->id.'-'.$report->user->id.'.jpg')}}" style="width: 100px;height:60px;" alt=""></a></span></p>
                        @if ($report->risolto==null||0)
                        <p>Risolto: <span style="color: red"><i class="fas fa-times-circle" style="font-size: 2rem;"></i></span></p>
                        @else
                        <p>Risolto: <span style="color: green"><i class="fas fa-check-circle" style="font-size: 2rem;"></i></span></p>
                        @endif
                    </li><hr>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>


        
    </x-layout>