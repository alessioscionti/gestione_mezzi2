<x-layout>

    <div class="container">
        <h1 class="text-center my-5">Cosa vuoi fare?</h1>
        <div style="height: 200px"></div>
        <div class="row my-5 mt-5 justify-content-center">
            <div class="col">
                <div id="f1_container">
                    <div id="f1_card" class="shadow">
                      <div class="front face">
                        <a href="{{route('veichles.index')}}">
                        <img src="{{asset('/storage/storage/jumpy_van.svg')}}"/>
                        </a>
                      </div>
                      <div class="back face text-center">
                        <a href="{{route('veichles.index')}}" class="iconhome"style="text-align: center;text-decoration:none;color:white">
                        <p>Mezzi</p>
                        </a>
                      </div>
                    </div>
                </div> 
            </div>
            <div class="col">
                <div id="f1_container">
                    <div id="f1_card" class="shadow">
                      <div class="front face">
                        <a href="{{route('event.index')}}">
                        <img src="{{asset('/storage/storage/chiave_inglese.svg')}}"/>
                        </a>
                      </div>
                      <div class="back face text-center">
                        <a href="{{route('event.index')}}" class="iconhome"style="text-align: center;text-decoration:none;color:white">
                        <p>Interventi</p>
                        </a>
                      </div>
                    </div>
                </div> 
            </div>
            <div class="col">
                <div id="f1_container">
                    <div id="f1_card" class="shadow">
                      <div class="front face">
                        <a href="{{route('report.index')}}">
                        <img src="{{asset('/storage/storage/notepad.svg')}}"/>
                        </a>
                      </div>
                      <div class="back face text-center">
                        <a href="{{route('report.index')}}" class="iconhome"style="text-align: center;text-decoration:none;color:white">
                        <p>Segnalazioni</p>
                        </a>
                      </div>
                    </div>
                </div> 
            </div>
            <div class="col">
                <div id="f1_container">
                    <div id="f1_card" class="shadow">
                      <div class="front face">
                        <a href="{{route('assicurazioni.index')}}">
                        <img src="{{asset('/storage/storage/insurance.svg')}}"/>
                        </a>
                      </div>
                      <div class="back face text-center">
                        <a href="{{route('assicurazioni.index')}}" class="iconhome"style="text-align: center;text-decoration:none;color:white">
                        <p>Assicurazioni</p>
                        </a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layout>