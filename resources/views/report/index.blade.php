<style>
    td{
        color: white;
    }
</style>
<x-layout>
    <div style="height: 200px"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 mt-5">
                <button class="btn btn-success"><a href="{{route('report.crea')}}" style="text-decoration: none;color:white">Registra Nuova Segnalazione</a></button>
            </div>
        </div>
    </div>
    @if (Auth::user()->worker==1)
        <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                @if (Session::has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
                </div>
            </div>
            <div class="row">
                  <div class="col-12">
                      <table class="table">
                          <thead>
                            <tr>
                                <th>Utente</th>
                                <th>Mezzo</th>
                                <th>Motivo segnalazione</th>
                                <th>Data segnalazione</th>
                                <th>Dettaglio segnalazione</th>
                                <th>Foto</th>
                                <th>Azioni</th>
                            </tr>
                        </thead>
                      <tbody>
                        @foreach ($reports as $report)
                        <tr>
                          <td>{{$report->user->name}}</td>
                          <td>{{$report->veichle->nome}}</td>
                          <td>{{$report->motivo_segnalazione}}</td>
                          <td>{{$report->data_segnalazione}}</td>
                          <td>{{$report->dettaglio_segnalazione}}</td>
                          <td><a href="{{asset('/storage/storage/'.$report->id.'/'.$report->id.'-'.$report->user->id.'.jpg')}}" data-lightbox="image-1" data-title="My caption"> <img src="{{asset('/storage/storage/'.$report->id.'/'.$report->id.'-'.$report->user->id.'.jpg')}}" style="width: 110px;height:60px;" alt=""></a></td>
                          <td><a href="{{route('report.edit',$report->id)}}"><i class="fas fa-edit" style="color:red"></i></a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
          </div>
          @else
          <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                @if (Session::has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
                </div>
            </div>
            <div class="row">
                  <div class="col-12">
                      <table class="table">
                          <thead>
                            <tr>
                                <th>Utente</th>
                                <th>Mezzo</th>
                                <th>Motivo segnalazione</th>
                                <th>Data segnalazione</th>
                                <th>Dettaglio segnalazione</th>
                                <th>Foto</th>
                                <th>Risolto</th>
                                <th>Km attuali</th>
                                <th>Azioni</th>
                            </tr>
                        </thead>
                      <tbody>
                        @foreach ($reports as $report)
                        <tr>
                          <td>{{$report->user->name}}</td>
                          <td>{{$report->veichle->nome}}</td>
                          <td>{{$report->motivo_segnalazione}}</td>
                          <td>{{$report->data_segnalazione}}</td>
                          <td>{{$report->dettaglio_segnalazione}}</td>
                          <td><a href="{{asset('/storage/storage/'.$report->id.'/'.$report->id.'-'.$report->user->id.'.jpg')}}" data-lightbox="image-1" data-title="My caption"> <img src="{{asset('/storage/storage/'.$report->id.'/'.$report->id.'-'.$report->user->id.'.jpg')}}" style="width: 110px;height:60px;border-radius:2px;" alt=""></a></td>
                          @if ($report->risolto==null||0)
                          <td><span style="color: red"><i class="fas fa-times-circle" style="font-size: 2rem;"></i></span></td>
                          @else
                          <td><span style="color: green"><i class="fas fa-check-circle" style="font-size: 2rem;"></i></span></td>
                          @endif
                          <td>{{$report->km_attuali}}</td>
                          @if (Auth::user()->id==$report->user->id||Auth::user()->admin==1)
                              
                          <td><a href="{{route('report.edit',$report->id)}}"><i class="fas fa-edit" style="color:red"></i></a></td>
                          @else
                          <td>--</td>
                          @endif
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
          </div>
        
    @endif
</x-layout>