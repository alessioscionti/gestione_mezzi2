<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <main id='main' class="mt-5">
        <h1 id='title'>Registra Nuova Segnalazione</h1>
        <p id='description'></p>
        <figure>
          <form id='survey-form' method="POST" action="{{route('report.store')}}" enctype="multipart/form-data">
            @csrf
            <label id='name-label'>Intervento per il veicolo:</label>
            <select name="veichle_id" id="">
                @foreach ($veichles as $veichle)
                <option value="{{$veichle->id}}">{{$veichle->nome}} - {{$veichle->targa}}</option>
                @endforeach
            </select>
            <select name="user_id" id="">
                @foreach ($users as $user)
                <option value="{{Auth::user()->id}}">{{Auth::user()->name}}</option>
                @endforeach
            </select>
            <hr>
            <label id='email-label'>Motivo Segnalazione</label>
            <input type='text' id='email' class='control' name="motivo_segnalazione" required>
            <hr>
            <label id='number-label'>Data Segnalazione</label>
            <input type='date' id='text' class='control' name="data_segnalazione" value="\Carbon\Carbon::now()" required>
            <hr>
            <label id='number-label'>Dettaglio Segnalazione</label>
            <input type='text' id='number' class='control' name="dettaglio_segnalazione" placeholder="spiega dettagliatamente la tua segnalazione" required>
            <hr>
            <label id='number-label'>carica immagini</label>
            <input type='file' id='number' class='control' name="immagine">
            <hr>
            <label id='number-label'>Km attuali</label>
            <input type='number' id='number' class='control' name="km_attuali" placeholder="inserisci i km del mezzo al momento della segnalazione" required>
            <hr>
            <button id="submit">Salva</button>
      
          </form>
        </figure>
      </main>


</x-layout>