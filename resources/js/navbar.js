/* var btnToggle = document.querySelector(".btn-toggle");
var btnToggleIcon = document.querySelector(".btn-toggle i");
var sidebar = document.querySelector(".sidebar");
var texts = document.querySelectorAll(".text");
btnToggle.addEventListener("click", (e) => {
    for (var text of texts) {
        if (!sidebar.classList.contains("sidebar-toggle")) {
            text.style.display = "none";
        } else {
            text.style.display = "block";
        }
    }
    btnToggleIcon.classList.toggle("icon-rotate")
    sidebar.classList.toggle("sidebar-toggle");
});

 */
// SHOW MENU
const showMenu = (toggleId, navbarId, bodyId) => {
    const toggle = document.getElementById(toggleId),
        navbar = document.getElementById(navbarId),
        bodypadding = document.getElementById(bodyId);

    if (toggle && navbar) {
        toggle.addEventListener("click", () => {
            // APARECER MENU
            navbar.classList.toggle("show");
            // ROTATE TOGGLE
            toggle.classList.toggle("rotate");
            // PADDING BODY
            bodypadding.classList.toggle("expander");
        });
    }
};
showMenu("nav-toggle", "navbar", "body");

// Change active link when clicked
const linkColor = document.querySelectorAll(".nav-link");

function colorLink() {
    linkColor.forEach((l) => l.classList.remove("active"));
    this.classList.add("active");
}

linkColor.forEach((l) => l.addEventListener("click", colorLink));